import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { ApisService } from 'src/app/services/apis.service';

@Component({
  selector: 'app-promotions',
  templateUrl: './promotions.page.html',
  styleUrls: ['./promotions.page.scss'],
})
export class PromotionsPage implements OnInit {
  allRest: any = [];

  constructor(private router: Router, private api: ApisService) { }

  ngOnInit() {
    this.api.getCoupons().then(data => {
      this.allRest = data;
    })
    // this.allRest = [{
    //   cover: 'https://www.tresjotas.com/wp-content/uploads/2018/03/saber-termino-de-la-carne.jpg',
    //   name: 'Solo por hoy',
    //   desc: '30% de descuento en todas nuestras carnes a la parrilla.',
    //   restaurant: "Carnes Parrilla",
    //   uid: 'iUBOSGVXstVysSa2HDnOGn2DEvM2'
    // },{
    //   cover: 'https://todosobreelasado.com/wp-content/uploads/2020/05/Como-hacer-pollo-a-la-parrilla-500x375.jpg',
    //   name: 'A comer pollo!',
    //   desc: '15% de descuento por compras superiores a $15.000 en pollos a la parrilla.',
    //   restaurant: "Carnes Parrilla",
    //   uid: 'iUBOSGVXstVysSa2HDnOGn2DEvM2'
    // },{
    //   cover: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSaZcS1-iTIq-PwYGzoedIyGeynXmghAPwga_rD5r9x53GoNEFqy8XyPUlQXLynBSOuprk&usqp=CAU',
    //   name: 'Miércoles de cumpleaños',
    //   desc: 'Al festejarlo con nosotros recibe 10% de descuento en tu parrilla.',
    //   restaurant: "Carnes Parrilla",
    //   uid: 'iUBOSGVXstVysSa2HDnOGn2DEvM2'
    // }];
  }

  openMenu(item) {
    const navData: NavigationExtras = {
      queryParams: {
        id: item.available[0].id
      }
    };
    this.router.navigate(['category'], navData);
  }

}
