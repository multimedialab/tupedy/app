import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FormRestaurantPageRoutingModule } from './form-restaurant-routing.module';

import { FormRestaurantPage } from './form-restaurant.page';
import { SharedModule } from 'src/app/directives/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FormRestaurantPageRoutingModule,
    SharedModule
  ],
  declarations: [FormRestaurantPage]
})
export class FormRestaurantPageModule {}
